package vn.com.fsoft.mtservice.bean.manager;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author hungxoan
 *
 * @param <T>
 */
@Component("importManager")
public class ImportManager<T extends Object> {

    private ImportManager _INSTANCE;
    private Map<Integer, List<T>> database = new HashMap<Integer, List<T>>();
    private Map<Integer, List<Object>> modelDatabase = new HashMap<Integer, List<Object>>();

    public ImportManager(){

        LoadingCache<Integer, List<T>> loadingCache =
                CacheBuilder.newBuilder()
                        .maximumSize(100) // maximum 100 records can be cached
                        .expireAfterAccess(10, TimeUnit.MINUTES) // cache will expire after 30 minutes of access
                        .build(new CacheLoader<Integer, List<T>>(){ // build the cacheloader

                            @Override
                            public List<T> load(Integer userId) throws Exception {
                                //make the expensive call
                                List<T> list = getFromDatabase(userId);
                                if(list == null){
                                    throw new Exception();
                                }
                                database.remove(userId);
                                return list;
                            }
                        });


        LoadingCache<Integer, List<T>> modelLoadingCache =
                CacheBuilder.newBuilder()
                        .maximumSize(100) // maximum 100 records can be cached
                        .expireAfterAccess(10, TimeUnit.MINUTES) // cache will expire after 30 minutes of access
                        .build(new CacheLoader<Integer, List<T>>(){ // build the cacheloader

                            @Override
                            public List<T> load(Integer userId) throws Exception {
                                //make the expensive call
                                List<T> list = getFromDatabase(userId);
                                if(list == null){
                                    throw new Exception();
                                }
                                modelDatabase.remove(userId);
                                return list;
                            }
                        });
    }

    public void putToDatabase(Integer userId, List<T> list){

        database.put(userId, list);
    }

    public void putToDatabase(Integer userId, List<Object> list, String reportLogic){

        List<Object> models = null;
        //HungPV comment this code
        //models = ImportCacheQueryCoordinator.get(reportLogic, list);

        modelDatabase.put(userId, models);
    }

    private List<T> getFromDatabase(Integer userId) {

        return database.get(userId);
    }

    private List<Object> getFromModelDatabase(Integer userId) {

        return modelDatabase.get(userId);
    }

    public List<T> getList(Integer userId){

        List<T> entityList = null;
        entityList = database.get(userId);

        return entityList;
    }

    public List<Object> getModelList(Integer userId){

        List<Object> entityList = null;
        entityList = modelDatabase.get(userId);

        return entityList;
    }

}
