package vn.com.fsoft.mtservice.bean.manager;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import vn.com.fsoft.mtservice.object.entities.ApplicationConfigEntity;
import vn.com.fsoft.mtservice.object.entities.MasterDataTableEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author hungxoan
 *
 */

@Component("applicationConfigManager")
public class ApplicationConfigManager {

    private SessionFactory sessionFactory;
    private HibernateTemplate hibernateTemplate;
    private Map<String, String> configs = new HashMap<String, String>();
    private Map<String, MasterDataTableEntity> configToMasterData = new
            HashMap<String, MasterDataTableEntity>();

    public ApplicationConfigManager(@Autowired SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        this.hibernateTemplate = new HibernateTemplate(sessionFactory);
        init();
    }

    private void init() {

        String hql = "select a from ApplicationConfigEntity a";
        List<ApplicationConfigEntity> entityList = null;

        try {
            entityList = (List<ApplicationConfigEntity>) hibernateTemplate.find(hql);
        } catch (DataAccessException ex) {
            // log ex
            return;
        }

        if(!CollectionUtils.isEmpty(entityList)) {
            for(ApplicationConfigEntity entity : entityList) {
                configs.put(entity.getKey(), entity.getValue());
            }
        }

        reloadMasterData();
    }

    public String get(String key) {
        return configs.get(key);
    }

    public MasterDataTableEntity getMaster(String key) {

        if(configToMasterData.containsKey(key)) {
            return configToMasterData.get(key);
        }

        reloadMasterData();
        return configToMasterData.get(key);
    }

    private synchronized void reloadMasterData() {

        List<String> configKeys = new ArrayList<String>();
        for(String configName : configs.values()) {
            configKeys.add(configName);
        }

        if(CollectionUtils.isEmpty(configKeys)) {
            return;
        }

        String hql = "select m from MasterDataTableEntity m where m.name in :names";
        List<MasterDataTableEntity> entityList = null;

        try {
            entityList = (List<MasterDataTableEntity>) hibernateTemplate
                    .findByNamedParam(hql, "names", configKeys);
        } catch (DataAccessException ex) {
            // log ex
            return;
        } catch (ObjectNotFoundException ex) {
            // log ex
            return;
        }

        if(CollectionUtils.isEmpty(entityList)) {
            return;
        }

        for(MasterDataTableEntity entity : entityList) {
            configToMasterData.put(entity.getName(), entity);
        }

    }
}
