package vn.com.fsoft.mtservice.controller;


import vn.com.fsoft.mtservice.bean.interceptor.RoleCheckingInterceptor;
import vn.com.fsoft.mtservice.bean.interceptor.TokenInterceptor;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.util.JacksonUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author hungxoan
 *
 */
public class BaseControllerTemplate {

    protected TokenInterceptor tokenInterceptor;
    protected RoleCheckingInterceptor roleCheckingInterceptor;

    public ResponseEntity<String> reject(ValidationStatus validationStatus) {

        String body = rejectDefinition(validationStatus);
        return new ResponseEntity<String>(body, null, HttpStatus.BAD_REQUEST);
    }

    protected String rejectDefinition(ValidationStatus validationStatus) {
        return JacksonUtils.java2Json(validationStatus);
    }

    public TokenInterceptor getTokenInterceptor() {
        return tokenInterceptor;
    }

    public void setTokenInterceptor(TokenInterceptor tokenInterceptor) {
        this.tokenInterceptor = tokenInterceptor;
    }

    public RoleCheckingInterceptor getRoleCheckingInterceptor() {
        return roleCheckingInterceptor;
    }

    public void setRoleCheckingInterceptor(RoleCheckingInterceptor roleCheckingInterceptor) {
        this.roleCheckingInterceptor = roleCheckingInterceptor;
    }

}
