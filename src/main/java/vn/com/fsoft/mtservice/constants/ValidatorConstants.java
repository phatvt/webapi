package vn.com.fsoft.mtservice.constants;

/**
 * hungxoan
 */
public class ValidatorConstants {
    public static final String CODE_REGEX = "[0-9a-zA-Z_]+";
    public static final String NAME_REGEX = "[0-9a-zA-Z_ ]+";
}
