package vn.com.fsoft.mtservice.constants;

/**
 * hungxoan
 */
public class CommonConstants {
    public static final String EMPTY = "";
    public static final String SINGLE_SPACE = " ";
    public static final String PERCENT = "%";
    public static final String UNDERLINE = "_";
    public static final String COMMIT = ",";
    public static final Integer ZERO = 0;

    public static final String STANDARD_SEARCH_TYPE = "Standard";
    public static final String REQUIREMENT_SEARCH_TYPE = "Requirement";
    public static final String COLLECTION_SEARCH_TYPE = "Collection";
    public static final String INTERNAL_TYPE = "Internal";
    public static final String EXTERNAL_TYPE = "External";
    
    public static final String IMPLEMENTATION = "ReportImplementation";
    public static final String FUNCTION = "Function";
}
