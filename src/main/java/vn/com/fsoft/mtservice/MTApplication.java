////////////////////////////////////////////////////////////
// MainApp.java
// Version: 1.0
// Author: (FPT) PhatVT1
// Create date: 2018/05/26
// Update date: -
// Update: HungPV
////////////////////////////////////////////////////////////
package vn.com.fsoft.mtservice;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;

import vn.com.fsoft.mtservice.bean.filter.CORSFilter;

@SpringBootApplication
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
public class MTApplication {
	final static Logger logger= LoggerFactory.getLogger(MTApplication.class); 
	
	@Value("${multipart.filesize.limit}")
	private String fileSize;
	
    public static void main(String[] args) {
        SpringApplication.run(MTApplication.class, args);
    }
    
 // Filters
 	@Bean
 	public Filter compressFilter() {
 		CORSFilter filter = new CORSFilter();
 		return filter;
 	}

     // Multiparts
 	@Bean
 	public MultipartConfigElement multipartConfigElement() {
 		MultipartConfigFactory factory = new MultipartConfigFactory();
 		factory.setMaxFileSize(fileSize);
 		return factory.createMultipartConfig();
 	}
}

