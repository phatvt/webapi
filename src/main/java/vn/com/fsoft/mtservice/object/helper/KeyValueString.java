package vn.com.fsoft.mtservice.object.helper;

import com.fasterxml.jackson.annotation.JsonView;

import vn.com.fsoft.mtservice.util.Views;

/**
 * 
 * @author hungxoan
 *
 */
public class KeyValueString {
    private String key;
    private String value;

    public KeyValueString() {
    }

    public KeyValueString(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @JsonView(Views.Public.class)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonView(Views.Public.class)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
