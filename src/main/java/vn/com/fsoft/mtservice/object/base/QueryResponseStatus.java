package vn.com.fsoft.mtservice.object.base;



import java.util.List;

import vn.com.fsoft.mtservice.object.entities.HibernateRootEntity;

/**
 * 
 * @author hungxoan
 *
 * @param <T>
 */
public class QueryResponseStatus<T extends HibernateRootEntity> extends Status {

    private Integer index;
    private Integer count;
    private String next;
    private List<T> data;
    private Object object;

    public QueryResponseStatus() {
        super();
    }

    public QueryResponseStatus(String code, String message) {
        super(code, message);
    }

    public QueryResponseStatus(String code, String message, Integer index) {
        super(code, message);
        this.index = index;
    }

    public QueryResponseStatus(String code, String message, String next, List<T> data) {
        super(code, message);
        this.data = data;
        this.next = next;
    }

    public QueryResponseStatus(String code, String message, Integer count, List<T> data) {
        super(code, message);
        this.count = count;
        this.data = data;
    }

    public QueryResponseStatus(String code, String message, List<T> data) {
        super(code, message);
        this.data = data;
    }

    public QueryResponseStatus(String code, String message, Integer count, Object object) {
        super(code, message);
        this.count = count;
        this.object = object;
    }

    public QueryResponseStatus(String code, String message, Object object) {
        super(code, message);
        this.object = object;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
