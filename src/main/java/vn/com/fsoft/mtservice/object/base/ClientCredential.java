package vn.com.fsoft.mtservice.object.base;

/**
 * 
 * @author hungxoan
 *
 */
public class ClientCredential {

    private Integer userId;
    private String token;
    private String nonce;

    public ClientCredential() {

    }

    public ClientCredential(Integer userId, String token, String nonce) {
        this.userId = userId;
        this.token = token;
        this.nonce = nonce;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
