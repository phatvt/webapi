package vn.com.fsoft.mtservice.object.form.command;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * 
 * @author hungxoan
 *
 */
@JsonIgnoreProperties( ignoreUnknown = true)
public class AuthorityCommandForm extends RestCommandForm {

    private Integer function;
    private List<Integer> permissions;

    public AuthorityCommandForm() {
    }

    public Integer getFunction() {
        return function;
    }

    public void setFunction(Integer function) {
        this.function = function;
    }

    public List<Integer> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Integer> permissions) {
        this.permissions = permissions;
    }
}
