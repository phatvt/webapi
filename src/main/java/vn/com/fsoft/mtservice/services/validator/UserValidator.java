package vn.com.fsoft.mtservice.services.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import vn.com.fsoft.mtservice.bean.data.UserRepo;
import vn.com.fsoft.mtservice.constants.ValidatorConstants;
import vn.com.fsoft.mtservice.object.base.RepoStatus;
import vn.com.fsoft.mtservice.object.base.ValidationStatus;
import vn.com.fsoft.mtservice.object.entities.UserEntity;
import vn.com.fsoft.mtservice.object.form.UserForm;
import vn.com.fsoft.mtservice.object.form.command.UserCommandForm;
import vn.com.fsoft.mtservice.object.form.query.UserQueryForm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author hungxoan
 *
 */
@Component("userValidator")
@DependsOn({"userRepo"})
public class UserValidator extends BaseValidator<UserForm>  {

    @Autowired
    private UserRepo userRepo;

    @Value("${date.format}")
    private String dateFormat;

    @Override
    public ValidationStatus rejectValue(UserForm form, String section) {

        ValidationStatus validationStatus = null;
        UserCommandForm commandForm = null;
        UserQueryForm queryForm = null;


        if (null != form) {

            commandForm = form.getModel();
            queryForm = form.getQuery();
            // check required fields

            if (section.equals("command")) {

                if(StringUtils.isBlank(form.getT())) {
                    return new ValidationStatus("400",
                            "Type of the form should not be empty");
                }

                switch (form.getT()) {
                    case "n":
                        validationStatus = rejectIfEmpty(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfUserNameNotWellForm(commandForm.getUserName());
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfUserNameExisted(commandForm.getUserName());
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfPhoneNotValid(commandForm.getPhone());
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfEmailNotValid(commandForm.getEmail());
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfExceedMaxLength(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfEmailExisted(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }
                        break;
                    case "u":

                        validationStatus = rejectIfEmpty(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfPhoneNotValid(commandForm.getPhone());
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfEmailNotValid(commandForm.getEmail());
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfUpdatedEmailExisted(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }

                        validationStatus = rejectIfExceedMaxLength(commandForm);
                        if(validationStatus != null) {
                            return validationStatus;
                        }
                        break;

                    default:
                        break;
                }
            } else if(section.equals("query")) {

                if(StringUtils.isNotEmpty(queryForm.getPhone())) {
                    validationStatus = rejectIfQueryPhoneNotValid(queryForm.getPhone());
                    if(validationStatus != null) {
                        return validationStatus;
                    }
                }

                if(StringUtils.isNotEmpty(queryForm.getEmail())) {
                    // validationStatus = rejectIfEmailNotValid(queryForm.getEmail());
                    validationStatus = rejectIfEmailQueryNotValid(queryForm.getEmail());

                    if(validationStatus != null) {
                        return validationStatus;
                    }
                }
            } else if(section.equals("bulk")) {
                validationStatus = rejectIfIdListEmpty(commandForm);
                if(validationStatus != null) {
                    return validationStatus;
                }
            }
        } else {

            validationStatus = new ValidationStatus("400", "form data is invalid.");
            return validationStatus;
        }

        return null;
    }

    //check userCommandForm is empty
    private ValidationStatus rejectIfEmpty(UserCommandForm commandForm) {

        if(commandForm == null) {
            return new ValidationStatus("400", "Form data is undefined.");
        }

        if (StringUtils.isEmpty(commandForm.getUserName())) {

            return new ValidationStatus("400", "User name should not empty.");
        }

        if (StringUtils.isEmpty(commandForm.getEmail())) {

            return new ValidationStatus("400", "Email should not empty.");
        }

        if (StringUtils.isEmpty(commandForm.getFullName())) {

            return new ValidationStatus("400", "Full name should not not empty.");
        }

        if (StringUtils.isEmpty(commandForm.getAddress())) {

            return new ValidationStatus("400", "Address should not not empty.");
        }

        if (StringUtils.isEmpty(commandForm.getCompany())) {

            return new ValidationStatus("400", "Company should not not empty.");
        }

        return null;
    }

    private ValidationStatus rejectIfIdListEmpty(UserCommandForm commandForm) {

        List<Integer> ids = commandForm.getIds();

        if(CollectionUtils.isEmpty(ids)) {
            return new ValidationStatus("400", "The record identifier list should not be empty.");
        }

        return null;
    }

    //check user name of user is existed
    private ValidationStatus rejectIfUserNameExisted(String userName) {

        if (StringUtils.isEmpty(userName)) {
            return new ValidationStatus("400", "Username should not be empty");
        }

        RepoStatus<List<UserEntity>> repoStatus = userRepo.findByCode(userName);

        if(repoStatus.getObject() != null) {
            return new ValidationStatus("400",
                    "User name is  existing. Please try again.");
        }

        return null;
    }

    //check user name of user is not existed
    private ValidationStatus rejectIfUserNameNotExisted(String userName) {

        if (StringUtils.isEmpty(userName)) {
            return new ValidationStatus("400", "Username should not be empty");
        }

        RepoStatus<List<UserEntity>> repoStatus = userRepo.findByCode(userName);

        if(repoStatus.getObject() == null) {
            return new ValidationStatus("400",
                    "User name is not existing. Please try again.");
        }

        return null;
    }

    private ValidationStatus rejectIfUserNameNotWellForm(String userName) {

        Pattern pattern = Pattern.compile(ValidatorConstants.CODE_REGEX);
        Matcher matcher = pattern.matcher(userName);

        if(!matcher.find()) {
            return new ValidationStatus("404",
                    "This code is not well-formed. Please choose another one");
        }

        return null;
    }

    //check day format
    private ValidationStatus rejectIfNotADate(String dob) {

        if(StringUtils.isEmpty(dateFormat)) {
            dateFormat = DEFAULT_DATE_FORMAT;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        try {

            simpleDateFormat.parse(dob);
        } catch(ParseException parseEx) {

            // log ex

            return new ValidationStatus("400", "Dob is not a valid date");
        }

        return null;
    }

    //check email format
    private ValidationStatus rejectIfEmailNotValid(String email) {

        if(StringUtils.isEmpty(email)) {
            return new ValidationStatus("400", "Email is not empty");
        }

        Pattern pattern = Pattern.compile(EMAIL_REGEX,  Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        if (!matcher.find()) {

            return new ValidationStatus("400", "Email is not in valid format.");
        }

        return null;
    }

    private ValidationStatus rejectIfEmailQueryNotValid(String email) {

        if(StringUtils.isEmpty(email)) {
            return new ValidationStatus("400", "Email should not be empty");
        }

        if(email.contains("\'") || email.contains("\"")){
            return new ValidationStatus("400", "Email is not in valid format.");
        }

        return null;
    }

    private ValidationStatus rejectIfAddressContainQuote(String address) {

        if(address.charAt(0) == '\'' ) {
            return new ValidationStatus("400", "Address should not contain quote.");
        }

        return null;
    }

    private ValidationStatus rejectIfEmailExisted(UserCommandForm commandForm) {

        String email = commandForm.getEmail();

        RepoStatus<UserEntity> status = userRepo.findByEmail(email);

        if(status.getObject() != null) {
            return new ValidationStatus("400", "Email is existing. Please try another one");
        }

        return null;
    }

    private ValidationStatus rejectIfUpdatedEmailExisted(UserCommandForm commandForm) {

        String email = commandForm.getEmail();

        RepoStatus<UserEntity> status = userRepo.findByEmail(email);

        UserEntity user = status.getObject();

        if(user != null) {

            if(!user.getId().equals(commandForm.getId())) {

                return new ValidationStatus("400", "Email is existing. Please try another one");
            }
        }

        return null;
    }

    private ValidationStatus rejectIfPhoneNotValid(String phone) {

        Pattern pattern = Pattern.compile(PHONE_REGEX,  Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(phone);

        if (!matcher.find()) {

            return new ValidationStatus("409", "Phone is not in valid format.");
        }

        return null;
    }

    private ValidationStatus rejectIfQueryPhoneNotValid(String phone) {

        Pattern pattern = Pattern.compile(PHONE_REGEX,  Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(phone);

        if (!matcher.find()) {

            return new ValidationStatus("202", "Phone is not in valid format.");
        }

        return null;
    }

    // check ids list is empty
    private ValidationStatus rejectIfIdListIsEmpty(List<Integer> ids) {

        if(ids == null || ids.size() == 0) {

            return new ValidationStatus("400", "Ids list of user is not empty.");
        }

        return null;
    }

    private ValidationStatus rejectIfExceedMaxLength(UserCommandForm commandForm) {

        if(commandForm.getUserName().length() > 20){
            return new ValidationStatus("409", "User name field should not exceed 20 characters");
        }

        if(commandForm.getEmail().length() > 50) {
            return new ValidationStatus("409",
                    "Email field should not exceed 50 characters");
        }

        if(commandForm.getFullName().length() > 50) {
            return new ValidationStatus("409",
                    "Full Name field should not exceed 50 characters");
        }

        if(commandForm.getPhone().length() < 10 || commandForm.getPhone().length() > 20) {
            return new ValidationStatus("409",
                    "Phone field should not exceed 50 characters");
        }

        if(commandForm.getAddress().length() > 300) {
            return new ValidationStatus("409",
                    "Address field should not exceed 300 characters");
        }

        if(commandForm.getCompany().length() > 50) {
            return new ValidationStatus("409",
                    "Company field should not exceed 50 characters");
        }

        return null;
    }
}
